from django.db import transaction
from rest_framework import serializers

from core.serializers import related_serializer_process
from user import models


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Address
        read_only_fields = ('id', 'full_address')
        fields = read_only_fields + (
            'street',
            'city',
            'state',
            'zip_code',
        )


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.User
        read_only_fields = ('id',)
        fields = read_only_fields + (
            'first_name',
            'last_name',
        )
        extra_kwargs = {'url': {'lookup_field': 'id'}}


class ParentUserSerializer(UserSerializer):
    address = AddressSerializer(allow_null=True, required=False)

    @transaction.atomic
    def create(self, validated_data):
        address_data = validated_data.pop('address')
        if not validated_data.get('parent'):
            address_serializer = related_serializer_process(
                AddressSerializer,
                address_data,
                self.context
            )
            validated_data['address'] = address_serializer

        instance = super().create(validated_data)
        return instance

    @transaction.atomic
    def update(self, instance, validated_data):
        if 'address' in validated_data:
            address_data = validated_data.pop('address')
            if not validated_data.get('parent'):
                address_obj = related_serializer_process(
                    AddressSerializer,
                    address_data,
                    self.context,
                    instance=instance.address
                )
                validated_data['address'] = address_obj
            else:
                validated_data['address'] = None

        return super().update(instance, validated_data)

    class Meta:
        model = models.User
        read_only_fields = ('id',)
        fields = read_only_fields + (
            'first_name',
            'last_name',
            'address'
        )
        extra_kwargs = {'url': {'lookup_field': 'id'}}


class ChildUserSerializer(UserSerializer):
    parent = serializers.PrimaryKeyRelatedField(
        many=False,
        queryset=models.User.objects.filter(parent__isnull=True)
    )

    class Meta:
        model = models.User
        read_only_fields = ('id',)
        fields = read_only_fields + (
            'first_name',
            'last_name',
            'parent',
        )
        extra_kwargs = {'url': {'lookup_field': 'id'}}
