import pytest

from django import urls

from user import models


@pytest.fixture
def parent_user_data():
    return {
        "first_name": "first name test",
        "last_name": "last name test",
        "address": {
            "street": "test street",
            "city": "Irvine",
            "state": "CA",
            "zip_code": "12436",
        }
    }


@pytest.fixture
def child_user_data():
    return {
        "first_name": "first name test",
        "last_name": "last name test",
        "parent": 1
    }


@pytest.fixture
def create_parent_user(client, parent_user_data):
    url = urls.reverse("user:parent-list")
    resp = client.post(
        path=url,
        data=parent_user_data,
        content_type="application/json"
    )
    return resp.json()
