import pytest

from django import urls

from user import models


@pytest.mark.django_db
def test_parent_creation(client, parent_user_data):
    assert models.User.objects.count() == 0
    url = urls.reverse("user:parent-list")
    resp = client.post(
        path=url,
        data=parent_user_data,
        content_type="application/json"
    )
    assert models.User.objects.count() == 1
    assert resp.status_code == 201


@pytest.mark.django_db
def test_child_creation_without_parent(client, child_user_data):
    url = urls.reverse("user:child-list")

    """- Child with parent = None(null) test -"""
    child_user_data = {**child_user_data, "parent": None}
    resp = client.post(
        path=url,
        data=child_user_data,
        content_type="application/json"
    )
    assert models.User.objects.filter(parent__isnull=False).count() == 0
    assert resp.status_code == 400
    """-Child without parent test-"""
    child_user_data.pop("parent")
    resp = client.post(
        path=url,
        data=child_user_data,
        content_type="application/json"
    )
    assert models.User.objects.filter(parent__isnull=False).count() == 0
    assert resp.status_code == 400


@pytest.mark.django_db
def test_child_creation(client, create_parent_user, child_user_data):
    url = urls.reverse("user:child-list")
    resp = client.post(
        path=url,
        data=child_user_data,
        content_type="application/json"
    )
    assert models.User.objects.filter(parent__isnull=False).count() == 1
    assert resp.status_code == 201
