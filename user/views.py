import logging

from rest_framework import filters as rest_filters
from rest_framework import permissions, viewsets, pagination

from user import serializers

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class UserBaseViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.AllowAny]
    filter_backends = (rest_filters.SearchFilter, rest_filters.OrderingFilter)
    pagination_class = pagination.LimitOffsetPagination
    ordering_fields = ['-created_at']
    lookup_field = 'id'


class ParentUserViewSet(UserBaseViewSet):
    serializer_class = serializers.ParentUserSerializer
    queryset = serializer_class.Meta.model.objects.all()

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(parent__isnull=True)
        return queryset


class ChildUserViewSet(UserBaseViewSet):
    serializer_class = serializers.ChildUserSerializer
    queryset = serializer_class.Meta.model.objects.all()

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(parent__isnull=False)
        return queryset
