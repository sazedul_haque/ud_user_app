from rest_framework import routers

from user import views

router = routers.SimpleRouter()
router.register('v1/user/parent', views.ParentUserViewSet, basename="parent")
router.register('v1/user/child', views.ChildUserViewSet, basename="child")
urlpatterns = router.urls
