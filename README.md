
---

## Task details

#### About (Story)
Create an APP that stores user data


[comment]: <> (Language can be Python or Java:)

[comment]: <> (Please include in the readme the python/Java version and how to install it)

[comment]: <> (How to run the project)

[comment]: <> (All standards must be respected)

### Requirements for implementation:
- Data composed of first name, last name, address (street, city, state and zip)
- The app should create the following User types (Parent, Child) The child cannot have an address and Must belong to a parent
- App should have API to:
	- Create user data
	- Update user data
	- Delete user data
- Data can be saved in a file or a DB (whichever is easy)
- Readme file describing how to install/run the application
- The project has to provide a mechanism that tests the application to guarantee that it works properly

### Solution built using: 

* Django and Python3, 
* Django Rest Framework, 
* SQLite3

### How to run: 

First we need to clone it from following repo:
- https://bitbucket.org/sazedul_haque/ud_user_app/src/master/

Here we included the db in our repository which is ``ud_test.sqlite3``
and also added `/conf/.env` file as it is a test project

You need to install python 3.9 [python download link](https://www.python.org/downloads/) 
After cloning it from repository you need to create run following command to install required packages.

```
pip install -r requirements.txt
```

```
python manage.py migrate
python manage.py runserver
```

Using this url you can create parent user

### URL for Parent
Using this url we can get list, create, update, delete Parent
```
GET/POST | http://127.0.0.1:8000/api/v1/user/parent/
PUT/PATCH/DELETE | http://127.0.0.1:8000/api/v1/user/parent/2(parent id)
```

### URL for Child
Using this url we can get list, create, update, delete Child
```
GET/POST | http://127.0.0.1:8000/api/v1/user/child/
PUT/PATCH/DELETE | http://127.0.0.1:8000/api/v1/user/child/2(child id)
```

### Example For Parent creation in python
```python
import requests
import json

url = "http://127.0.0.1:8002/api/v1/user/parent/"

payload = json.dumps({
  "first_name": "Shahidul",
  "last_name": "Haque",
  "address": {
    "street": "Stree no 5",
    "city": "Irvine",
    "state": "CA",
    "zip_code": "12345"
  }
})
headers = {
  'Content-Type': 'application/json'
}

response = requests.request("POST", url, headers=headers, data=payload)

print(response.text)
```
### Testing
For testing run following command in root directory and we will get feedback like

```commandline
pytest
```

```commandline
user/test/test_users_process.py::test_parent_creation Creating test database for alias 'default'...PASSED
user/test/test_users_process.py::test_child_creation_without_parent PASSED
user/test/test_users_process.py::test_child_creation PASSED
================================================================= 3 passed in 39.36s ==================================================================
```



