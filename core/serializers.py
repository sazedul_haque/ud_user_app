__all__ = [
    'related_serializer_process',
]


def related_serializer_process(related_serializer, data, context, instance=None):
    if instance:
        serializer = related_serializer(instance, data=data, context=context)
    else:
        serializer = related_serializer(data=data, context=context)
    serializer.is_valid(raise_exception=True)
    serializer.save()

    return serializer.instance
